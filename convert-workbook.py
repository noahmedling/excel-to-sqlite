'''
Convert an Excel workbook to SQLite.
'''

import codecs
import itertools
import re
import sqlite3
import sys

import openpyxl

def slugify(text, lower=True):
    if lower:
        text = text.strip().lower()
    text = re.sub(r'[^\w _-]+', '', text)
    text = re.sub(r'[- ]+', '_', text)
    return text

def quote_identifier(s, errors="strict"):
    encodable = s.encode("utf-8", errors).decode("utf-8")

    nul_index = encodable.find("\x00")

    if nul_index >= 0:
        error = UnicodeEncodeError('NUL-terminated utf-8', encodable, nul_index, nul_index + 1, 'NUL not allowed')
        error_handler = codecs.lookup_error(errors)
        replacement, _ = error_handler(error)
        encodable = encodable.replace('\x00', replacement)

    return '"' + encodable.replace('"', '""') + '"'

def load_tables(workbook, database):
    table_names = set()
    for sheet in workbook.worksheets:
        table_name = slugify(sheet.title)
        if table_name in table_names:
            i = 2
            while ('%s%d' % (table_name, i)) in table_names:
                i += 1
            table_name = '%s%d' % (table_name, i)
        table_names.add(table_name)

        column_names = [cell.value for cell in next(sheet.rows)]

        print('%s: %r' % (table_name, column_names))

        print('Creating table %s...' % table_name)
        create_query = 'CREATE TABLE %s (%s);' % (
            quote_identifier(table_name),
            ', '.join(['%s TEXT' % quote_identifier(name) for name in column_names]))
        database.execute(create_query)

        insert_query = 'INSERT INTO %s (%s) VALUES (%s);' % (
            quote_identifier(table_name),
            ', '.join([quote_identifier(name) for name in column_names]),
            ', '.join(['?' for name in column_names]))
        insert_data = [
            [str(cell.value).strip() if cell.value is not None else '' for cell in row]
            for row in itertools.islice(sheet.rows, 1, None)]
        print('Inserting %d rows into %s...' % (len(insert_data), table_name))
        database.executemany(insert_query, insert_data)

def main():
    if len(sys.argv) != 3:
        print('Usage: %s <workbook> <database>' % sys.argv[0], file=sys.stderr)
        return -1
    workbook_filename = sys.argv[1]
    database_filename = sys.argv[2]
    workbook = openpyxl.load_workbook(workbook_filename)
    try:
        with sqlite3.connect(database_filename) as database:
            load_tables(workbook, database)
            return 0
    finally:
        workbook.close()

if __name__ == '__main__':
    sys.exit(main())
