# Convert Excel workbooks to SQLite databases

## Usage

```
$ python convert-workbook.py workbook.xslx database.sqlite
```
